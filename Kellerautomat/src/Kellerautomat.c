#include <stdio.h>
#include <stdlib.h>

/* Dieser Spezialfall eines Kellerautomaten ueberprueft,
 * ob alle Klammern geschlossen sind.
 *
 * @author: Frank Denzer
 */

const int N = 100;
int n_used = 0;

char pop(char **stack) {
	if (n_used > 0) {
		n_used--;
		return *(--(*stack));
	} else {
		exit(1);
	}
}

void push(char **stack, char c) {
	n_used++;
	// avoid error by exiting
	if (n_used >= N) {
		exit(1);
	} else {
			*((*stack)++) = c;
	}
}

int main() {

	char *stack = (char *) malloc(sizeof(char) * N);

	printf("Zu testende Kette aus Klammern eingeben: ");

	int wrong = 0;
	char c;

	push(&stack, '\0');

	while (((c = getchar()) != '\n') && !wrong) {
		switch (c) {
		case '(':
			push(&stack, c);
			break;
		case ')':
			if (pop(&stack) != '(')
				wrong = 1;
			break;
		default:
			break;
		}
	}

	if (!wrong && pop(&stack) != '\0') {
		wrong = 1;
	}

	if (wrong) {
		printf("\nFalsche Klammerung\n");
	} else {
		printf("\nAusdruck korrekt\n");
	}

	return 0;
}
